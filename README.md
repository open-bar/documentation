# Open bar documentation

## Architecture

```mermaid
flowchart TB
  subgraph Pulsar
  end
  subgraph Backend
    db[(PostgreSQL)]
    subgraph Spring
        API[API Rest]
        Projections[Projections]
        Websocket
    end
    Projections -- écriture --> db[(PostgreSQL)]
    Websocket -- lecture --> db[(PostgreSQL)]
    API -- lecture --> db[(PostgreSQL)]
  end
  Pulsar -- subscribe --> Projections
  Pulsar -- subscribe --> Websocket
  API -- publish --> Pulsar
  subgraph fronts
    Caisse[Logiciel de caisse]
    Admin[Interface d'admin]
  end
  Admin --> API
  Caisse --> API
  Caisse <--> Websocket
  Caisse <--> TPE
```

### Pourquoi une architecture event-driven ?
Le gros avantage d'une architecture event-driven concerne la règlementation des logiciels de caisse. En effet, l'ensemble des actions affectant une transaction doivent être concervée afin de limiter les risques de fraudes à la TVA. Avec une architecture event-driven, qui est pas définition en *append only* c'est-à-dire qu'on ne fait que ajouter des évènements, il n'y a ni suppression ni modification possible des données. 

Un autre avantage est la facilité à séparer les lectures des écritures, et appliquer ainsi les principes *CQRS* ce qui permet d'améliorer le passage à l'échelle de l'application. 

Un désavantage de ce type d'architecture est sont aspect *eventially consistent*, c'est-à-dire que la cohérence des données n'est ganrantie qu'à un moment donnée et non immédiatement comme avec une base de données *ACID*. 

### Quelques principes
#### Séparation des responsabilités
Ou *Separation of Concerns* abbrégé *SoC* est un principe qui consiste en séparer en différents domaines les différentes parties de l'application. 

Dans notre cas, on pourra par exemple dans le cadre de notre *MVP* identifier trois domaines : 
- **L'identité** ou les parties prenantes : va concerne la gestion des utilisateurs, des organisations et des permissions ;
- **Le catalogue** : concerne la gestion des produits et des catégories ;
- **La transaction** : concerne l'ensemble du processus d'encaissement, de la gestion du panier courant jusqu'au paiement. 

Par la suite d'autres domaines peuvent être ajoutés comme par exemple la comptabilité, les statistiques, la gestion de stock, etc. 

La séparation des responsabilité en domaine permet généralement de mieux définir les inferfaces de programmation et d'encapsuler les éventuelles complexité ou détails d'implémentation. Il permet généralement d'avoir un code plus simple, plus clair et plus lisible.

#### Idempotence
Le rôle des projecteurs est de créer les vues qui permettent d'exposer les données via l'API ou le websocket. Le projecteur s'abonne à un ensemble d'évènements sur un ou plusieurs topic, et écrit dans une base de données l'état correspondant à l'ensemble des évènements qu'il a traité. 

Un principe fondamental à appliquer lors de l'implémentation d'un projecteur est l'idempotence (*idempotency* en anglais). Ce principe consiste à ce qu'une opération ait le même effet qu'on l'applique une ou plusieurs fois. Dans notre cas, ça implique que si le projecteur reçoit plusieurs fois le même évènement, on obtiendra le même état que s'il était reçu qu'une fois. 

Ce principe s'applique simplement en stockant le messageId dans une base de données associée au projecteur, permettant ainsi de ne pas appliquer le même évènement deux fois. Une implémentation plus fine consiste soit à insérer le messageId dans le cadre d'une transaction, soit insérer le messageId associé à un statut, et mettre à jour le status une fois que l'évènement est traité (ou au contraire supprimer, ou mettre dans un état d'erreur si l'évènement n'a pas pu être correctement traité.)

#### Observabilité
Permettre une bonne observabilité de ce qu'il se passe au sein du système est fondamental pour debugger des certaines situations. Puisqu'on est ici dans un système asynchrone, il est fondamental de pouvoir suivre le flux des évènements dans les logs. Pour cela, on peut mettre en place une bonne pratique qui consiste à associer à chaque requête entrante un ID de corrélation (*correlationId*) qui sera propagé à l'ensemble du système, notamment via les évènements envoyés à Pulsar. Ce *correlationId* ajouté aux logs permettra de suivre l'exécution d'une requête de bout en bout très facilement.

#### Les exceptions c'est le mal
Classiquement, une manière simple de gérer un cas d'erreur (par exemple : il manque une donnée dans une requête envoyée à l'API). Mais cette manière de faire est en réalité très peu pratique, car la signature d'une fonction ou d'une méthode ne donne pas l'information des exceptions qui peuvent être déclenchées. Et donc, lorsqu'on utilise cette fonction, il peut arriver qu'on oublie de gérer les cas d'erreurs possibles. 

Une manière d'éviter cela est d'utiliser un *design pattern* appelé *Result Pattern*. Il consiste à renvoyer une structure `Result`. Cet objet a deux états possibles : il contient soit le type de retour attendu de la fonction, soit une erreur. Cette manière de faire est beaucoup plus pratique parce que d'une part ça informe les utilisateurs de cette fonction des cas d'erreurs possibles, d'autre part ça implique que ces cas d'erreurs soit typés et donc plus facile à gérer et enfin ça oblige à gérer le cas d'erreur (ou en tout cas à l'ignorer volontairement).

#### Principes pour le front
L'ensemble de ces principes s'applique aussi au front mais quelques principes additionnels permettent de concevoir un meilleur produit.

##### Injection de dépendences
L'injection de dépendences permet pour le front d'abstraire l'ensemble des problématiques spécifiques aux différents domaine, masque les détails d'implémentation et facilite l'exécution de tests. 

Par exemple, plutôt que de définir un client d'API qui va exécuter des requêtes en renvoyer le résultat pour accéder au catalogue, on va définir une interface permettant d'accéder au catalogue. Cette interface pourra être implémentée par différentes classes. De base on fera souvent deux implémentations : 
- une implémentation réalisant effectivement les appels à l'API et renvoyant les résultats associés
- une implémentation *in memory* nous permettant d'utiliser l'interface sans backend ou bien de faire tourner des tests fonctionnels. 


## Modèle de données
Étant donné l'architecture choisie, ce modèle de données n'est décrit qu'à titre indicatif, pour bien se représenter les éventuelles relations entre les différents objets. Ça n'implique pas nécessairement que ce soit effectivement la représentation choisie en base de données. 

En effet, il pourrait apparaitre plus pertinent de choisir une base de données non relationnelle de type document oriented par exemple, notamment pour représenter le catalogue et ses produits à destination du logiciel de caisse. C'est d'ailleurs aussi l'avantage d'une architecture event-driven, c'est qu'on peut construire des vues dédiées pour chaque front qui n'ont pas forcément les mêmes besoins. 

```mermaid
---
title: Utilisateur et Organisations
---
classDiagram
    class Organization {
        id: UUID
        name: string

    }
    class User {
        id: UUID
        username: string UNIQUE @ forbidden in username
        email: string UNIQUE
        pasword: string | argon2 hash
    }
    class OrganizationMembership {
        id: UUID
        permissions | role
    }
    User <|-- OrganizationMembership
    Organization <|-- OrganizationMembership
    User <|-- Organization
```

```mermaid
---
title: Catalogue
---
classDiagram
    class Screen {
        id: UUID
        name: string
        order: string
        active: boolean
        parent?: Screen
        image?: string
        color?: string
    }
    class AccountingGroup {
        id: UUID
        name: string
        vatRate: number
    }
    Screen --|> Screen
    Screen --|> Organization
    Product --|> Organization
    class Product {
        id: UUID
        name: string
        purchasePrice: number
        sellPrice: number
        image?: string
        freePrice?: boolean = false
    }
    class ProductScreen {
        id: UUID
        order: string
    }
    ProductScreen --|> Screen
    ProductScreen --|> Product
    Product --|> AccountingGroup
    AccountingGroup --|> Organization

    class CashType {

    }
    class MeanOfPayment {
        name: string
    }
```

Au sujet de la représentation des montant monétaires. On pourra utiliser comme convention de les représenter sous la forme de BigInteger en milliers de centimes. Donc 1 € sera représenté par `100 000`. Ceci permet d'éviter les approximations liés à la représentation des float, tout en ayant une résolution suppérieure aux centimes. 

Dans un premier temps, nous n'inclueons pas la currency dans le modèle. Il pourra par la suite être mis accolé à chaque montant sans la forme d'une structure `{amount: 100 000, currency: "EUR"}`. 

De même, pour les taux de TVA, on pourra les représenter en milliers de pourcentage (ou en pour cent-mille). C'est à dire qu'une TVA à 5,5% sera représentée par le nombre `5 500`.


```mermaid
---
title: Encaissements
---
classDiagram
    class PointOfSale {
        id: UUID
        name: string
    }
    class Cart {
        id: UUID
        status: IN_PROGRESS | ON_HOLD | CLOSED | PAID | REFUNDED
        closedReason?: string
    }
    class Article {
        id: UUID
        productId: UUID
        productName: string
        productPurchasePrice: BigInteger
        productSellPrice: BigInteger
        vatRate: number
    }
    class Payment {
        id: UUID
        status: IN_PROGRESS | SUCCESS | ERROR
        amount: BigInteger
        date: timestamp
        errorReason?: string
    }
    class Discount {
        id: UUID
        amount?: BigInteger
        percentage?: number
    }
    PointOfSale --|> Organization
    Cart --|> PointOfSale
    Article --|> Cart
    Payment --|> Cart
    Article <|.. Discount
    Cart <|.. Discount
```

## Exemple de flux de données
Cas d'usage : une caisse est en cours d'utilisation et on modifie depuis l'interface d'admin le prix d'un produit. 

```mermaid
sequenceDiagram
    autonumber
    participant Admin as Interface d'admin
    participant API
    participant Pulsar
    participant Projector
    participant Websocket
    participant Caisse as Logiciel de caisse

    Caisse -) Websocket: subscribe
    activate Websocket
    Admin -) API: PUT /products/$productId {"price": "320000"}
    activate API
    API -) Pulsar: new event `{"type": "ProductUpdate", "payload": {"price": "320000"}}
    API -) Admin: success 200
    Pulsar -) Projector: event `{"type": "ProductUpdate", "payload": {"price": "320000"}}
    Pulsar -) Websocket: event `{"type": "ProductUpdate", "payload": {"price": "320000"}}
    Websocket -) Caisse: productUpdate: {product payload}
    Caisse -) Caisse: Mise à jour du prix affiché + affichage d'un toast pour prévenir de la modification
```
Alternative avec persitence synchrone :

```mermaid
sequenceDiagram
    autonumber
    participant Admin as Interface d'admin
    participant API
    participant Pulsar
    participant Projector
    participant Websocket
    participant Caisse as Logiciel de caisse

    Caisse -) Websocket: subscribe
    activate Websocket
    Admin -) API: PUT /products/$productId {"price": "320000"}
    activate API
    API -) Pulsar: new event `{"type": "ProductUpdate", "payload": {"price": "320000"}}
    API -) Projector: updateProduct {"price": "320000"}
    activate Projector
    Projector -) API : product: [productPayload]
    API -) Admin: success 200 [productPayload]
    Pulsar -) Projector: event `{"type": "ProductUpdate", "payload": {"price": "320000"}} -> idempotency = do nothing
    Pulsar -) Websocket: event `{"type": "ProductUpdate", "payload": {"price": "320000"}}
    Websocket -) Caisse: productUpdate: {product payload}
    Caisse -) Caisse: Mise à jour du prix affiché + affichage d'un toast pour prévenir de la modification
```

## License
TBD

